package mvc.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import mvc.services.interfaces.GeolocationServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Muhammed
 * @since 2/4/2021.
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class GeolocationController {

    @Autowired
    private GeolocationServiceInterface geolocationServiceInterface;

    @GetMapping("/north-countries")
    public ResponseEntity<?> getLocations(@RequestParam List<String> ip) throws JsonProcessingException {
        if (ip.size() > 5 || ip.size() == 0)
            return new ResponseEntity<>("The input ips list cannot contain more than 5 or less then 1.",
                    HttpStatus.BAD_REQUEST);

            return geolocationServiceInterface.getCountryNames(ip);
    }

}
