package mvc.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mvc.services.domain.GeolocRequest;
import mvc.services.domain.ResponseDomain;
import mvc.services.domain.IpAPIResponse;
import mvc.services.interfaces.GeolocationServiceInterface;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * @author Muhammed
 * @since 2/4/2021.
 */

@Service
public class GeolocationServiceImpl implements GeolocationServiceInterface {

    @Override
    public ResponseEntity<?> getCountryNames(List<String> ips) throws JsonProcessingException {
        IpAPIResponse[] ipAPIResponse = this.getAPIResponses(ips);
        ResponseDomain responseDomain = new ResponseDomain();
        List<String> northCountries = responseDomain.getNorthcountries();

        //unique country names.
        Set<String> countriesSet = getUniqueCountyNames(ipAPIResponse);
        //sorting.
        northCountries.addAll(countriesSet);
        Collections.sort(northCountries);

        return new ResponseEntity<>(responseDomain, HttpStatus.OK);
    }

    private IpAPIResponse[] getAPIResponses(List<String> ips) throws JsonProcessingException {
        final String uri = "http://ip-api.com/batch";
        RestTemplate restTemplate = new RestTemplate();
        List<GeolocRequest> requests = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        ips.forEach(ip -> requests.add(new GeolocRequest(ip, "country,lat", "en")));
        ResponseEntity<String> response = restTemplate.postForEntity(uri, requests, String.class);
        String jsonInput = response.getBody();

        return objectMapper.readValue(jsonInput, IpAPIResponse[].class);
    }

    private Set<String> getUniqueCountyNames(IpAPIResponse[] ipAPIResponse){
        Set<String> countriesSet = new HashSet<>();

        for (IpAPIResponse respDomain : ipAPIResponse) {
            if (respDomain.getLat() >= 0)
                countriesSet.add(respDomain.getCountry());
        }
        return countriesSet;
    }
}
