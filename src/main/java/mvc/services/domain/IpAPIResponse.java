package mvc.services.domain;

/**
 * @author Muhammed
 * @since 2/4/2021.
 */
public class IpAPIResponse {

    private String country;
    private float lat;

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }
}
