package mvc.services.domain;

/**
 * @author Muhammed
 * @since 2/4/2021.
 */
public class GeolocRequest {
    private String query;
    private String fields;
    private String lang;

    public GeolocRequest(String query, String fields, String lang) {
        this.query = query;
        this.fields = fields;
        this.lang = lang;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
