package mvc.services.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Muhammed
 * @since 2/4/2021.
 */
@Transactional
public interface GeolocationServiceInterface {
    /**
     * get country names of northern hemisphere.
     *
     * @param ips input
     * @return list of names in response entity object.
     */
    ResponseEntity<?> getCountryNames(List<String> ips) throws JsonProcessingException;
}
