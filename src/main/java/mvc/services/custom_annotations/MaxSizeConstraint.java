package mvc.services.custom_annotations;

import mvc.services.validators.MaxSizeConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author Muhammed
 * @since 2/5/2021.
 */
@Documented
@Constraint(validatedBy = MaxSizeConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MaxSizeConstraint {

    String message() default "The input list cannot contain more than 5 ips.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
