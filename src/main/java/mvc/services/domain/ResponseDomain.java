package mvc.services.domain;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Muhammed
 * @since 2/5/2021.
 */
public class ResponseDomain {

    private List<String> northcountries;

    public ResponseDomain() {
        this.northcountries = new ArrayList<>();
    }

    public List<String> getNorthcountries() {
        return northcountries;
    }

    public void setNorthcountries(List<String> northcountries) {
        this.northcountries = northcountries;
    }
}
