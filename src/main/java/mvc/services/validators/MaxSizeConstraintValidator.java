package mvc.services.validators;

import mvc.services.custom_annotations.MaxSizeConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

/**
 * @author Muhammed
 * @since 2/5/2021.
 */
public class MaxSizeConstraintValidator implements ConstraintValidator<MaxSizeConstraint, List<String>> {

    @Override
    public boolean isValid(List<String> stringList, ConstraintValidatorContext constraintValidatorContext) {
        return stringList.size() <= 5 && stringList.size() > 0;
    }

    @Override
    public void initialize(MaxSizeConstraint constraintAnnotation) {

    }
}
