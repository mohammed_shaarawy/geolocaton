package org.example.GeoLocation;

import mvc.controllers.GeolocationController;
import mvc.services.impl.GeolocationServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {GeolocationController.class, GeolocationServiceImpl.class})
public class GeoLocationApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeoLocationApplication.class, args);
    }


}
